// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
// Using the array you just obtained from the previous problem, 
//find out how many cars were made before the year 2000 and return the array of older cars and log its length.


const inventory = require("./Inventory.js");
const carYearsArray = require("./problem4.js");

function CarOlderThan2000 (carYearsArray){

    const OlderCar =[]
    
    for(let i =0;i<carYearsArray.length;i++){

        if(carYearsArray[i] < 2000){
            OlderCar.push(carYearsArray[i])
        }
    }

    return OlderCar;
}

const oldCars = CarOlderThan2000(carYearsArray(inventory))
console.log(`cars older that 2000 are`)
console.log(oldCars)

module.exports = CarOlderThan2000;