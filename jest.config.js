module.exports = {
    moduleFileExtensions: ["js"],
    testEnvironment: "node",
    transform: {
      "^.+\\.(js|jsx|mjs)$": "babel-jest",
    },
  };
  