// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order and log the results
// in the console as it was returned.

const inventory = require("./Inventory.js"); 

function sortCarModelsAlphabetically(inventory) {
  
  const carModels = [];
  for (let i = 0; i < inventory.length; i++) {
    carModels.push(inventory[i].car_model);
  }
  
  carModels.sort();

  return carModels;
}

const sortedCarModels = sortCarModelsAlphabetically(inventory);

console.log("Car models sorted alphabetically:");
console.log(sortedCarModels);

module.exports = sortCarModelsAlphabetically;

