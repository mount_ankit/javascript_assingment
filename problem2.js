// The dealer needs the information on the last car in their inventory. 
//Execute a function to find what the make and model of the last car in the inventory is? 
// Log the make and model into the console in the format of:


const inventory = require("./Inventory.js");

const lent = inventory.length;

function findLastCar(inventory){

    if(inventory.length ==0){
        return "inventory is empty"
    }

    const lastCar = inventory[inventory.length-1]

    const carModel = lastCar.car_model;
    const carMade = lastCar.car_make;
    
    return {carMade, carModel}
}

const lastCarInfo = findLastCar(inventory);

if(typeof(lastCarInfo) === 'string'){
    console.log(lastCarInfo)
}
else{
    console.log(`last card made by the company was ${lastCarInfo.carMade} and its was of ${lastCarInfo.carModel} model`)
}

module.exports = findLastCar;