const { expect } = require("chai");
const CarOlderThan2000 = require("../problem5.js")
const  allYears = require("../problem4.js");
const inventory = require("../Inventory.js");

describe("it will give cars older than 2000",()=>{
    it("should get me all the car year before 2000", ()=>{
        const oldCars = CarOlderThan2000(allYears(inventory))
        expect(oldCars).to.deep.equal(
            [
                1983, 1990, 1995, 1987, 1996,
                1997, 1999, 1987, 1995, 1994,
                1985, 1997, 1992, 1993, 1964,
                1999, 1991, 1997, 1992, 1998,
                1965, 1996, 1995, 1996, 1999
              ]
        )
    })


})