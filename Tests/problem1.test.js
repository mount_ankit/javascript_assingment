// testProblem1.js

const { expect } = require("chai");
const findCarById = require("../problem1.js");

const inventory = require("../Inventory.js")

describe("findCarById", () => {
    it("should return car information for id 33", () => {
        
        const car33 = findCarById(33);
        expect(car33).to.deep.equal(inventory[33-1]);
    });


});
