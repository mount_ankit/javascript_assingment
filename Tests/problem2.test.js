const { expect } = require("chai");
const findLastCar = require("../problem2.js")
const inventory = require("../Inventory.js")


describe("LastCar", () => {
    
    it("should reutrn name of car maker and car model ",() => {
        const Lastcar = findLastCar(inventory);
        expect(Lastcar).to.deep.equal(
            {"carMade": "Lincoln", "carModel": "Town Car"})
    })

});