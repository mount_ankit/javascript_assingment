// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. 
//Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
//Then log the car's year, make, and model in the console log in the format of:
const inventory = require("./Inventory.js");

function findCarById(id) {
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id === id) {
            return inventory[i];
        }
    }
    return null;
}

const car33 = findCarById(33);

if (car33) {
    let car_year = car33.car_year;
    let car_make = car33.car_make;
    let car_model = car33.car_model;

    console.log("Car 33 is a " + car_year + " made by " + car_make + " and its model is " + car_model);
} else {
    console.log("Car 33 not found in the inventory.");
}

module.exports = findCarById;